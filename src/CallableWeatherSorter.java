import java.util.LinkedList;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 *  A callable class that takes a ConcurrentLinkedList and goes through and finds the top 5 WeatherData objects. This
 *  class keeps running until the ConcurrentLinkedList is exhausted. The class then returns a linkedList containing the
 *  top/bottom 5 results in a LinkedList of WeatherData objects.
 */

public class CallableWeatherSorter implements Callable<LinkedList<WeatherData>> {
    private Boolean min;
    private ConcurrentLinkedQueue<WeatherData> queue;


    CallableWeatherSorter(Boolean min,ConcurrentLinkedQueue<WeatherData> queue){
        this.min = min;
        this.queue = queue;
    }

    /**
     *  call() method used by the Callable interface. This is where almost all the work of the class is done. This method
     *  will grab a WeatherData, compare it to the topFive array and will add it to the array if the WeatherData is larger/smaller
     *  then anything in the list (depending on the status of min). After the ConcurrentLinkedQueue is empty, it will
     *  add the top/bottom 5 into a linked list and return it.
     * @return A linked list representing the top/bottom 5 results.
     * @throws Exception
     */
    public LinkedList<WeatherData> call() throws Exception{
        WeatherData[] topFive = new WeatherData[5];
        LinkedList<WeatherData> returnFive = new LinkedList<WeatherData>();

        while(!queue.isEmpty()){
            WeatherData wData = queue.poll();
            if(!(wData == null)){
                Boolean found = false;
                for(int i=0; i<= 4; i++){
                    if(topFive[i]==null && !found) {
                        topFive[i] = wData;
                        found = true;
                    } else if(!found) {
                        if (min) {
                            if (wData.getValue() < topFive[i].getValue()) {
                                WeatherData temp = topFive[i];
                                topFive[i] = wData;
                                wData = temp;
                            }
                        } else {
                            if (wData.getValue() > topFive[i].getValue()) {
                                WeatherData temp = topFive[i];
                                topFive[i] = wData;
                                wData = temp;
                            }
                        }
                    }
                }
            }
        }
        for(int i = 0; i<=4; i++){
            if(!(topFive[i] == null)){
                returnFive.add(topFive[i]);
            }

        }
        return returnFive;
    }
}
