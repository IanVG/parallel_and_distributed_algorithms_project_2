public class StationData {

    private String id;
    private float latitude;
    private float longitude;
    private float elevation;;
    private String state;
    private String name;

    /**
     *  Object that holds the information for a station. This method contains only getters and setters and a toString.
     *
     * @param id String that represents the ID of the station
     * @param latitude Float that represents the latitude of the station
     * @param longitude Float that represents the longitude of the station
     * @param elevation Float the represents the elevation of the station
     * @param state String that represents the state of the station.
     * @param name String that represents the name of the station.
     */
    public StationData(String id, float latitude, float longitude, float elevation, String state, String name) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
        this.elevation = elevation;
        this.state = state;
        this.name = name;
    }
    public StationData(){

    }

    /**
     * Overridden toString that is used to print information about the station.
     *
     * @return String of info about the station.
     */
    @Override
    public String toString() {
        return  "id='" + id + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", elevation=" + elevation +
                ", state=" + state +
                ", name=" + name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getElevation() {
        return elevation;
    }

    public void setElevation(float elevation) {
        this.elevation = elevation;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
