import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Main class used to launch the program. It has a easy to edit fileDirectory that will be passed to the rest of the program
 */
public class Main {
    private static final String fileDirectory = "C:\\weatherData";

    /**
     *  Main method that fires when the program is launched for the first time. Constructs a new ConcurrentLinkedQueue
     *  and passes a reference to the fileDirectory.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        ConcurrentLinkedQueue<WeatherData> queue = new ConcurrentLinkedQueue<WeatherData>();
        Controller controller = new Controller(queue,fileDirectory);
    }
}
