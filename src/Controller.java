import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.*;

/**
 *  The controller is a single threaded controller that creates and distributes the tasks to the multi-threaded components
 *  of the program. In addition, it handles all the single threaded components of this program.
 */
public class Controller {

    public Boolean min;
    public int startYear;
    public int endYear;
    public int startMonth;
    public int endMonth;
    public String fileDirectory;
    public LinkedList<File> files = new LinkedList<File>();
    public LinkedList<WeatherData>  finalQueue = new LinkedList<WeatherData>();
    public ConcurrentLinkedQueue<WeatherData> queue;

    /**
     *  Constructor for the Controller. This class is passed a ConcurentLinkedQueue and the file directory that has the files
     *  located in it. This constructor will go through each part of the program in order.
     * @param queue A ConcurrentLinkedQueue that is used in findTopFiveReduced()
     * @param fileDirectory A string that has the location of all the weather data.
     * @throws IOException
     */

    public Controller(ConcurrentLinkedQueue<WeatherData> queue,String fileDirectory) throws IOException {

        this.queue = queue;
        this.fileDirectory = fileDirectory;
        getUserInput();
        getFiles(fileDirectory);
        findTopFiveAll();
        findTopFiveReduced();
        findTopFiveFinal();
    }

    /**
     * Prompts the user for information regarding what data they want to get. It asks the user for StartYear, EndYear,
     * StartMonth, EndMonth, and if they want the TMIN or TMAX.
     */
    public void getUserInput() {
        String s;
        boolean valid = false;

        Scanner reader = new Scanner(System.in);
        System.out.println("Would you like to get the maximum or minimum temperatures?");
        System.out.println("Type MAX for maximum and MIN for minimum and then hit ENTER");
        s = reader.next().toLowerCase();
        if (s.equals("max") || s.equals("min")) {
            valid = true;
        }
        while (valid == false) {
            System.out.println("I'm sorry, I don't understand.");
            System.out.println("Please type MAX for maximum and MIN for minimum and then hit ENTER");
            s = reader.next().toLowerCase();
            if (s.equals("max") || s.equals("min")) {
                valid = true;
            }
        }
        if (s.equals("max")) {
            min = false;
        } else {
            min = true;
        }
        System.out.println("What year do you want to start at? Please print full year.");
        startYear = getNumber(1800, 2018, reader);

        System.out.println("What month do you want to start at? Please print number of month");
        startMonth = getNumber(1, 12, reader);

        System.out.println("What year do you want to end at? Please print full year.");
        endYear = getNumber(startYear, 2018, reader);

        System.out.println("What month do you want to end at? Please print full month.");
        endMonth = getNumber(1, 12, reader);
    }

    /**
     * Prompts the user for a number input between the low and high bounds, returns an int with the response
     *
     * @param lowBound  Int of lowest bound of number
     * @param highBound Int of highest bound of number
     * @param reader    Reader of input
     * @return Int between the low and high bounds
     */
    private int getNumber(int lowBound, int highBound, Scanner reader) {
        boolean valid;
        String s;
        int i = 0;
        s = reader.next();
        if (s.matches("-?\\d+")) {
            i = Integer.parseInt(s);
        }
        valid = (i >= lowBound) && (i <= highBound);
        while (valid == false) {
            System.out.println("I'm sorry, I don't understand. Please try again.");
            s = reader.next();
            if (s.matches("-?\\d+")) {
                i = Integer.parseInt(s);
            }
            valid = (i >= lowBound) && (i <= highBound);
        }
        return i;
    }

    /**
     * Lookes in the path\\ghcnd_hcn directory and creates a linked list containing a File object for every file located
     * inside the directory.
     * @param path The path the the folder containing the unpacked \\ghcnd_hcn
     */
    private void getFiles(String path) {

        File folder = new File(path + "\\ghcnd_hcn");
        File[] listOfFiles = folder.listFiles();

        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                files.push(listOfFile);
            }
        }
    }

    /**
     *  This method creates a future of CallableWeatherReader for every file in the listOfFiles and puts it into a linkedList.
     *  This future is given a reference to the StartYear, EndYear, StartMonth, EndMonth and aa boolean representing either TMAX or TMIN.
     *  It then creates an ThreadPool with 15 threads to go through the futures. As the futures return, it adds the 5 WeatherData
     *  objects to the ConcurrentLinkedList. Once all the futures have been exhausted, it shuts down the threadpool.
     */
    private void findTopFiveAll() {
        ExecutorService executor = Executors.newFixedThreadPool(15);
        List<Future<LinkedList<WeatherData>>> list = new ArrayList<Future<LinkedList<WeatherData>>>();

        while (!files.isEmpty()) {
            Callable<LinkedList<WeatherData>> callable = new CallableWeatherReader(files.pop(), startYear, endYear, startMonth, endMonth, min);
            Future<LinkedList<WeatherData>> future = executor.submit(callable);
            list.add(future);
        }
        for (Future<LinkedList<WeatherData>> future : list) {
            try {
                LinkedList<WeatherData> data = future.get();
                    while (!data.isEmpty()) {
                        if(!(data.peek() == null)){
                            queue.add(data.pop());
                        } else {
                            data.pop();
                        }
                    }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdown();
    }

    /**
     * This method creates 4 new futures to iterate over the ConcurrentLinkedList provided by findTopFiveAll(). These futures are
     * given a reference to the ConcurrentLinkedList and a boolean representing either max or min. Once the ConcurrentLinkedList is
     * exhausted, it takes the 5 WeatherData objects from each of the 4 futures and adds it to finalQueue.
     */

    private void findTopFiveReduced() {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        List<Future<LinkedList<WeatherData>>> list = new ArrayList<Future<LinkedList<WeatherData>>>();

        for(int x=0; x<4; x++){
            Callable<LinkedList<WeatherData>> callable = new CallableWeatherSorter(min, queue);
            Future<LinkedList<WeatherData>> future = executor.submit(callable);
            list.add(future);
        }
        for (Future<LinkedList<WeatherData>> future : list) {
            try {
                LinkedList<WeatherData> data = future.get();
                while (!data.isEmpty()) {
                    if(!(data.peek() == null)){
                        finalQueue.add(data.pop());
                    } else {
                        data.pop();
                    }
                }
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
        executor.shutdown();
    }

    /**
     * This is the single threaded class that goes through the 20 WeatherData objects located in finalQueue and finds either
     * the highest or lowest 5. It then looks up the StationData for the top/bottom 5. It then prints the top/bottom 5 WeatherData
     * along with the associated StationData.
     *
     * @throws IOException
     */
    private void findTopFiveFinal() throws IOException {
        WeatherData[] topFive = new WeatherData[5];

        while(!finalQueue.isEmpty()){
            WeatherData wData = finalQueue.poll();
            if(!(wData == null)){
                Boolean found = false;
                for(int i=0; i<= 4; i++){
                    if(topFive[i]==null && !found) {
                        topFive[i] = wData;
                        found = true;
                    } else if(!found) {
                        if (min) {
                            if (wData.getValue() < topFive[i].getValue()) {
                                WeatherData temp = topFive[i];
                                topFive[i] = wData;
                                wData = temp;
                            }
                        } else {
                            if (wData.getValue() > topFive[i].getValue()) {
                                WeatherData temp = topFive[i];
                                topFive[i] = wData;
                                wData = temp;
                            }
                        }
                    }
                }
            }
        }

        File file = new File(fileDirectory + "\\ghcnd-stations.txt");
        FileReader read = new FileReader();
        StationDataProcessor process = new StationDataProcessor(read.ReadStationFile(file));

        for(int x=0; x<=4; x++){
            System.out.println(topFive[x]);
            System.out.println(process.FindStationById(topFive[x].getId()));
        }

    }


}

