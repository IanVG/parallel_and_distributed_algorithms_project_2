import java.util.LinkedList;

/**
 * This class contains methods that can take lists of WeatherData and do work on them. The two methods in this class are
 * used to find the smallest or largest five entries in the passed lists.
 */
public class WeatherDataProcessor {


    /**
     *  A method that takes a linkedList of WeatherData and finds the five with the largest value. It then returns
     *  a linkedList of the five largest WeatherData's
     * @param wList The linkedList of WeatherData that needs to be sorted
     * @return A linkedList of the five largest WeatherData objects
     */
    public LinkedList<WeatherData> findTopFive(LinkedList<WeatherData> wList){
        WeatherData[] topFive = new WeatherData[5];
        LinkedList<WeatherData> topFiveList = new LinkedList<WeatherData>();

        while(!wList.isEmpty()){
            WeatherData wData = wList.pop();
            Boolean found = false;
                for (int i = 0; i <= 4; i++) {
                    if (topFive[i] == null && !found) {
                        topFive[i] = wData;
                        found = true;
                    } else if (!found) {
                        if (wData.getValue() > topFive[i].getValue()) {
                            WeatherData temp = topFive[i];
                            topFive[i] = wData;
                            wData = temp;
                        }
                    }
                }
        }

        for(int i=0; i<=4; i++){
            topFiveList.add(topFive[i]);
        }
        return topFiveList;
    }

    /**
     *  A method that takes a linkedList of WeatherData and finds the five with the smallest value. It then returns
     *  a linkedList of the five smallest WeatherData's
     * @param wList The linkedList of WeatherData that needs to be sorted
     * @return A linkedList of the five smallest WeatherData objects
     */

    public LinkedList<WeatherData> findBottomFive(LinkedList<WeatherData> wList){
        WeatherData[] topFive = new WeatherData[5];
        LinkedList<WeatherData> topFiveList = new LinkedList<WeatherData>();

        while(!wList.isEmpty()){
            WeatherData wData = wList.pop();
            Boolean found = false;
            for(int i=0; i<= 4; i++){
                if(topFive[i]==null && !found) {
                    topFive[i] = wData;
                    found = true;
                } else if(!found) {
                    if(wData.getValue()<topFive[i].getValue()){
                        WeatherData temp = topFive[i];
                        topFive[i] = wData;
                        wData = temp;
                    }
                }
            }
        }

        for(int i=0; i<=4; i++){
            topFiveList.add(topFive[i]);
        }
        return topFiveList;
    }

}
