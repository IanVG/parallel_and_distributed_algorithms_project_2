import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * This class contains all the logic needed to read a file and then decode it into its WeatherData or StationData object.
 */
public class FileReader {

    /**
     *  This class takes a file and a list of requirements and creates WeatherData from the files.
     *  First it look at the File file and pulls out each line of the document, one at a time creating a string. It then passes
     *  that string to DecodeWeatherData() which will split the string into WeatherData objects. It then takes the LinkedList<WeatherData>
     *  from DecodeWeatherData and returns it. Only WeatherData inside the bounds of StartYear/StartMonth and EndYear/EndMonth are returned.
     *
     * @param file The weather file that needs to be read and decoded
     * @param startYear The start year bound
     * @param endYear The end year bound
     * @param startMonth The start month bound
     * @param endMonth The end month bound
     * @param min Boolean represeting if the TMAX or TMIN need to be returned. True for TMIN, Faluse for TMAX.
     * @return A linked list of weatherData that is inside the bounds.
     * @throws IOException
     */
    public LinkedList<WeatherData> ReadWeatherFile(File file, int startYear, int endYear, int startMonth, int endMonth, Boolean min) throws IOException {
        String selectedElement;
        LinkedList<WeatherData> weatherList = new LinkedList<>();
        LinkedList<String> lines = new LinkedList<String>();
        if(min == true){
            selectedElement = "TMIN";
        } else {
            selectedElement = "TMAX";
        }

        Scanner scanner = new Scanner(file);

        while (scanner.hasNext()) {
            lines.add(scanner.nextLine());
        }

        while (!lines.isEmpty()){
            LinkedList<WeatherData> tempData = DecodeWeatherData(lines.pop());
            while(!tempData.isEmpty()){
                WeatherData wData =  tempData.pop();
                if(wData.getElement().equals(selectedElement)){
                    int wDataYear = wData.getYear();
                    Boolean approve = false;
                    if(wDataYear>=startYear && wDataYear<=endYear){
                        approve = true;
                        if(wDataYear == startYear) {
                            if(wData.getMonth()<startMonth){
                                approve = false;
                            }
                        } else if(wDataYear == endYear){
                            if(wData.getMonth()>endMonth){
                                approve = false;
                            }
                        }
                        if(wData.getValue() == -9999){
                            approve = false;
                        }
                        if(!wData.getQflag().equals(" ")){
                            approve = false;
                        }
                    }
                    if(approve){
                        weatherList.add(wData);
                    }
                }
            }
        }
        return weatherList;
        }

    /**
     *  This method takes a string representing a single line of the weather data file and decodes the data into WeatherData objects.
     *
     * @param input a string representing a single line of the weather data file.
     * @return A linkedList of Weather data containing all the data from the input.
     */

    private LinkedList<WeatherData> DecodeWeatherData(String input){
            LinkedList<WeatherData> tempList = new LinkedList<>();

            String id = input.substring(0,11);
            int year = Integer.valueOf(input.substring(11,15).trim());
            int month = Integer.valueOf(input.substring(15,17).trim());
            String element = input.substring(17,21);
            int days = (input.length() - 21) / 8; // Calculate the number of days in the line
            for (int i = 0; i < days; i++) {         // Process each day in the line.
                WeatherData wd = new WeatherData();
                wd.setDay(i + 1);
                int value = Integer.valueOf(input.substring(21+8*i,26+8*i).trim());
                String qflag = input.substring(27+8*i,28+8*i);
                wd.setId(id);
                wd.setYear(year);
                wd.setMonth(month);
                wd.setElement(element);
                wd.setValue(value);
                wd.setQflag(qflag);
                wd.toString();
                tempList.add(wd);
            }
            return tempList;
        }


    /**
     *  This class takes a file of Station information and grabs each line. It then passes each line to DecodeStationData
     *  to create a StationData object. This class returns all the stationData from the file in a linkedList
     *
     * @param file The data file that needs to be read and decoded
     * @return A linked list of all of the StationData from the file.
     * @throws IOException
     */

    public LinkedList<StationData> ReadStationFile(File file) throws IOException {
        LinkedList<StationData> StationList = new LinkedList<>();
        LinkedList<String> lines = new LinkedList<String>();

        Scanner scanner = new Scanner(file);

        while (scanner.hasNext()) {
            lines.add(scanner.nextLine());
        }

        while (!lines.isEmpty()) {
         StationList.add(DecodeStationData(lines.pop()));
        }
         return StationList;
    }

    /**
     *  Takes a single line of the station data file as a string and decodes the information from it and then creates
     *  a StationData object that is passed back.
     *
     * @param input a single line of the station data file
     * @return a StationData object that contains the data from the input
     */
    private StationData DecodeStationData(String input){
        StationData returnData = new StationData();

        returnData.setId(input.substring(0,11));
        returnData.setLatitude(Float.valueOf(input.substring(12,20).trim()));
        returnData.setLongitude(Float.valueOf(input.substring(21,30).trim()));
        returnData.setLatitude(Float.valueOf(input.substring(31,37).trim()));
        returnData.setElevation(Float.valueOf(input.substring(31,37).trim()));
        returnData.setState(input.substring(38,40));
        returnData.setName(input.substring(41,71));

        return returnData;
    }

}


