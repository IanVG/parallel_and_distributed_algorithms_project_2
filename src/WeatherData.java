/**
 * Class that holds the information from a single day of weather data. This class only contains getters setters and a toString
 *  method
 */
public class WeatherData {

    private String id;
    private int year;
    private int month;
    private int day;
    private String element;
    private int value;
    private String qflag;
    private StationData sData;


    /**
     *  Constructor that holds the information from a single day of weather data.
     *
     * @param id String representing the ID of the station that took the mesurement
     * @param year Int representing the year a reading was taken in. In format of 19XX
     * @param month Int representing the month a reading was taken in. In format of 1, and 12
     * @param day Int representing the day a reading was taken
     * @param element String representing the type of data taken
     * @param value Int representing the value of the element taken
     * @param qflag String extra information about measurement, usually an error code of some sort.
     */
    public WeatherData(String id, int year, int month, int day, String element, int value, String qflag) {
        this.id = id;
        this.year = year;
        this.month = month;
        this.day = day;
        this.element = element;
        this.value = value;
        this.qflag = qflag;
    }

    /**
     * Empty constructor if needed.
     */
    public WeatherData() {

    }

    /**
     * Overridden toString method for printing information about the WeatherData.
     *
     * @return String representing the WeatherData data
     */
    @Override
    public String toString() {
        double newValue = value;
        return  "id='" + id + '\'' +
                " year=" + year +
                " month=" + month +
                " day=" + day +
                " element='" + element + '\'' +
                " value=" + newValue/10+"C";
    }

    public StationData getsData() {
        return sData;
    }

    public void setsData(StationData sData) {
        this.sData = sData;
    }

    public String getId() {
        return id;
    }


    public void setId(String id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getQflag() {
        return qflag;
    }

    public void setQflag(String qflag) {
        this.qflag = qflag;
    }
}

