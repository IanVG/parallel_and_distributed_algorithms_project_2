import java.io.File;
import java.util.LinkedList;
import java.util.concurrent.Callable;

/**
 *  CallableWeatherReader is a callable class that will take a file as input and return a linked list of all the weatherdata
 *  days that are contained in the file.
 */
public class CallableWeatherReader implements Callable<LinkedList<WeatherData>> {
    File file;
    Boolean min;
    int startYear;
    int endYear;
    int startMonth;
    int endMonth;

    /**
     * Constructor for the CallableWeatherReader
     * @param file The reference to the file that needs to be read
     * @param startYear The start year bound
     * @param endYear The end year bound
     * @param startMonth The start month bound
     * @param endMonth The end month bound
     * @param min Boolean representing if the TMAX or TMIN need to be returned. True for TMIN, False for TMAX.
     */

    CallableWeatherReader(File file, int startYear,int endYear,int startMonth,int endMonth ,Boolean min){
        this.file = file;
        this.min = min;
        this.startYear = startYear;
        this.endYear = endYear;
        this.startMonth = startMonth;
        this.endMonth = endMonth;
    }

    /**
     *  Call method used by callable. This method takes the passed file and sends it to the filereader, it then takes the
     *  the returned linkedList and sends it to the WeatherDataProcessor. Then it takes the returned LinkedList<WeatherData>
     *  and returns it.
     * @return A LinkedList<WeatherData> containing the top or bottom five WeatherData objects from the file that was read.
     * @throws Exception
     */

    public LinkedList<WeatherData> call() throws Exception{
    FileReader read = new FileReader();
    LinkedList<WeatherData> fileList= read.ReadWeatherFile(file,startYear,endYear,startMonth,endMonth,min);

    WeatherDataProcessor process = new WeatherDataProcessor();
    LinkedList<WeatherData> fiveList;
        if(min){
        fiveList = process.findBottomFive(fileList);
    } else {
        fiveList = process.findTopFive(fileList);
    }
        return fiveList;
    }
}
