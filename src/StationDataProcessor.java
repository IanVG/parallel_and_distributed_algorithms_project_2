import java.util.LinkedList;

public class StationDataProcessor {

    LinkedList<StationData> sData;

    /**
     * Contains methods that are used to evaluate the LinkedList of StationData. Currently only has one method,
     * FindStationByID();
     *
     * @param sData a linked list of StationData
     */
    public StationDataProcessor(LinkedList<StationData> sData) {
        this.sData = sData;
    }

    /**
     *  This method searches through the linked list and finds and returns the StationData of the station with the id
     *  equal to the passed id. If it can not find the information, it returns null.
     * @param id the string ID of the station needed to be found.
     * @return a StationData object of ID, or null if none exist.
     */
    public StationData FindStationById(String id){
        for (StationData temp : sData) {
            if(id.equals(temp.getId())){
                return temp;
            }
        }
        return null;
    }

}
